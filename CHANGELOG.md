# Changelog

Changes beyond v1.10.0 are summarised here. For older changes, see [this page](https://gitlab.com/maxigaz/gitlab-dark/-/tags).

## v1.15.1

- Language ratio bar on project page: removed white areas
- Groups and subgroups page: fixed borders and text
- Account settings: fixed “Social sign-in” buttons

## v1.15.0

- Releases page
	- Styled card header on releases page
	- Fixed pagination buttons (also affects Projects page)
- Username popover
	- Fixed dark text
	- Altered remaining text colour
- Markdown previews
	- Styled hr in Markdown previews
	- Made header border brighter
- Miscellaneous
	- Made general hr brighter
	- Fixed border of Author filter button on Commits page
	- Fixed Linked issues and Related MRs under issue post
	- Fixed History button text colour on project page
	- Styled blog posts (navbar, body, sticky banner)

## v1.14.1

- Styled text and icon on Designs tab on Issue details page
- Styled info text on snippets page
- Styled keyboard shortcuts overlay (accessible through help icon on navbar)

## v1.14.0

- Notifications
	- Styled notifications appearing in the lower right corner and promotional messages (thanks @esse)
	- Made text on the message “Ready to be merged automatically. Ask someone with write access…” brighter
- Issue details page (under OP)
	- Fixed icon on the button for adding reactions as well as the help icon next to it
	- Fixed Linked issues section
- Project page
	- Fixed dropdown menu after clicking on button with “+” sign
	- Fixed Web IDE button text colour
- Commits page: Styled Author search dropdown menu

## v1.13.0

- Improvements for the IDE diff and file editor
- Styled repository graph page
- Fix for navigation items under “More”

## v1.12.0

- Issue details
	- Fixed background of info about issue title change
	- Fixed borders around “quick reply” and collapsed messages
	- Fixed dark timestamps
	- Fixed links pointing to user profiles and milestones
	- Styled OP edit details
	- Styled text in related issues
- GitLab Docs
	- Styled visited links
	- Styled section describing which version a feature was introduced in
	- Styled blockquote text colour
	- Inverted colour of heading anchors

## v1.11.2

- You can now specify three different custom domains at the same time.
- GitLab Docs
	- Styled left sidebar (full mode)
	- Styled active Table of Contents items
	- Specified text colour for important notes and tips to make them legible

## v1.11.1

- GitLab Docs
	- Tweaked border colour under level 1 headings
	- Specified text colour for notes to make it legible
	- Styled/tweaked Help and feedback

## v1.11.0

- Added [UserCSS option](https://github.com/openstyles/stylus/wiki/Manager#usercss-options) for specifying custom domain
- GitLab Docs
	- Replaced `url-prefix` rules for gitlab.com with a single `domain` rule to include [docs.gitlab.com](https://docs.gitlab.com/) as well
	- Styled left sidebar (collapsed mode)
- Activity page
	- Styled “Compare \<commit id1\>…\<commit id2\>” links
	- Styled event icons that looked a bit dark
